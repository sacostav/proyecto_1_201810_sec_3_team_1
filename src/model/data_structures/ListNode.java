package model.data_structures;

public class ListNode<T extends Comparable<T>> {

	
	T item;
	
	ListNode<T> siguiente;
	
    ListNode<T> anterior;
    
    int posicion;
    
    
    public ListNode(T elemento){
    	
    }
    
    public T darElemento(){
    	return item;
    }
    
    public void cambiarSiguiente(ListNode<T> pSiguiente){
    	
    	siguiente = pSiguiente;
    }
    public void cambiarAnterior(ListNode<T> pAnterior){
    	
    	anterior = pAnterior;
    }
    
    
    public ListNode<T> darAnterior(){
    	
    	return anterior;
    }
    
    public ListNode<T>darSiguiente(){
    	return siguiente;
    	}
    
    public int posicion(){
    	return posicion;
    }
    
    public void cambiarPosicion(int num){
    	posicion = num;
    }
    }
