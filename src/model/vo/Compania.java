package model.vo;

import java.text.ParseException;
import java.util.Date;

import model.data_structures.Iterator;
import model.data_structures.LinkedList;
import model.data_structures.Queue;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private LinkedList<Taxi> taxisInscritos;	
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(LinkedList<Taxi> taxisInscritos) {
		this.taxisInscritos = taxisInscritos;
	}

	
	public Taxi taxiMayorServicios(String fechaI, String fechaF) throws ParseException{
		
		int comp = 0;
		Taxi retornar = null;
		
		for(int i=0; i<taxisInscritos.size();i++){
			
			int numServicios = taxisInscritos.get(i).serviciosEnRango(fechaI, fechaF).size();
			
			if(numServicios>comp){
				
				comp = numServicios;
				retornar = taxisInscritos.get(i);
			}
			
		}
		
		return retornar;
	}
	
	/**
	 * @throws ParseException 
	 * 
	 */
	
	public LinkedList<Servicio> taxisRango(String dateI, String dateF) throws ParseException{

		
		LinkedList<Servicio> retornar = new LinkedList<Servicio>();
		for(int i=0; i<taxisInscritos.size();i++){
			
			Queue<Servicio> servicios = taxisInscritos.get(i).serviciosEnRango(dateI, dateF);		
			Iterator<Servicio> iterador = servicios.iterator();
			Servicio act = null;
			
			while(iterador.hasNext()){
				
				act = iterador.Next();
				retornar.add(act);
				
			}
		}
		
		return retornar;
	}
	
	/**
	 * 
	 */
	public Taxi taxiMasRentable(){
		
		double distancia = 0;
		double ganancia = 0;
		Taxi masRentable = null;
		
		for(int i=0;i<taxisInscritos.size();i++){
			
			if(taxisInscritos.get(i).totalGanacias()>ganancia&&taxisInscritos.get(i).totalDistancias()<distancia){
				
				masRentable = taxisInscritos.get(i);
				distancia = taxisInscritos.get(i).totalDistancias();
				ganancia = taxisInscritos.get(i).totalGanacias();
			}
		}
		
		return masRentable;
	}
	public Taxi taxiMayorPorFecha(String in, String fi)
	{
		Taxi t = null;
		double d = 0;
		for(int i = 0; i< taxisInscritos.size(); i++)
		{
			Taxi actual = taxisInscritos.get(i);
			try {
				Queue<Servicio> servicios = actual.serviciosEnRango(in, fi);
				if(servicios != null)
				{
					for(int j = 0; j < servicios.size() ;j++ )
					{
						double x = 0;
						Iterator<Servicio >it = servicios.iterator();
						while(it.hasNext())
						{
							Servicio s = it.Next();
							x += s.getTripTotal();
						}
						if(x > d)
						{
							d = x;
							t = actual;
						}
					}
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return t;
	}
	
	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return 0;
		
	}
	
	public Date convertirFecha(String fecha){
		return null;
	}

	public Taxi buscarTaxiId(String id){
		
		Taxi retornar = null;
		
		for(int i=0; i<taxisInscritos.size();i++){
			
			if(taxisInscritos.get(i).getTaxiId().equals(id)){
				
				retornar = taxisInscritos.get(i);
			}
		}
		
		return retornar;
	}

	public int serviciosIniciados(String dateI,String dateF) throws ParseException {
		
		
		int servicios = 0;
		Queue<Servicio> cola = new Queue<Servicio>();
		Iterator<Servicio> iterator = cola.iterator();
		
		for(int i=0; i<taxisInscritos.size();i++){
			
			cola = taxisInscritos.get(i).serviciosEnRango(dateI, dateF);
			Servicio act = null;
		    
			while(iterator.hasNext()){
	             
				act = iterator.Next();
		        servicios++;
				}

			
			
		}
		return servicios;
	}
	
	
	

}
