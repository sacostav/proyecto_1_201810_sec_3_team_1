package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	/**
	 * 
	 */
	private String fechaInicial;
	
	/**
	 * 
	 */
	private String fechaFinal;
	
	/**
	 * 
	 */
	
	private String identificador;
	
	/**
	 * 
	 */
	
	private ZonaServicios zonaInicio;
	
	/**
	 * 
	 */
	
	private double distancia;
	
	/**
	 * 
	 */
	
	private ZonaServicios zonaFinal;
	
	/**
	 * 
	 */
	private double costo;
	
	
	public Servicio(){
		
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return identificador;
	}	
	


	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return distancia;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return costo;
	}

	/**
	 * 
	 */
	
	public String fechaI(){
		return fechaInicial;
	}
	
	/**
	 * 
	 */
	public String fechaF(){
		return fechaFinal;
	}
	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
